import { products } from "./API.js";
import { generateProduct } from "./Product.js";

const catalog = document.getElementsByClassName("catalog")[0];

const generateProductsHTML = (productList) => {
  let productHTML = " ";
  for (const product of productList) {
    productHTML += generateProduct(product);
  }
  return productHTML;
};

// catalog
const fillUpCatalog = async (sort = null) => {
  const productList = await products(sort);
  catalog.innerHTML = generateProductsHTML(productList);
};

fillUpCatalog();

// sort
const sort = document.getElementById("sort");
sort.addEventListener("change", () => {
  console.log(sort.value);
  fillUpCatalog(sort.value);
});

//Search
const searchQuery = document.getElementById("searchQuery");
const searchButton = document.getElementById("searchButton");

searchButton.addEventListener("click", () => {
  products(sort).then((result) => {
    const filerProducts = result.filter(
      (product) => product.title.indexOf(searchQuery.value) != -1
    );
    catalog.innerHTML = generateProductsHTML(filerProducts);
  });
});
