export const generateProduct = (product) => `<div class="catalog__product">
<div class="catalog__photo">
  <img src="${product.image}" />
</div>
<div class="catalog__title">
 ${product.title}
</div>
<div class="catalog__prices">
  ${product.price} $
</div>
</div>`;
